import paho.mqtt.client as mqtt
import json
import time
import argparse
from tinydb import TinyDB, where


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("margot/+/welcome/#")
    client.subscribe("agora/+/knowledge")

def on_message(client, userdata, msg):
    exchanged = json.loads(msg.payload.decode("utf-8"))
    app_id = msg.topic.split("/")[1]
    name = app_id.split("^")[0]
    version = app_id.split("^")[1]
    block = app_id.split("^")[2]

    db = TinyDB("db/knowledges.json")
    if "agora" in msg.topic:
        entries = db.search((where("name") == name) & (where("version") == version))
        if len(entries) > 0:
            db.update(exchanged, (where("name") == name) & (where("version") == version))
        else:
            entry = dict()
            entry["name"] = name
            entry["version"] = version
            entry["knowledge"] = exchanged
            db.insert(entry)
    else:
        t_id = msg.topic.split("/")[3]
        entries = db.search((where("name") == name) & (where("version") == version))
        if len(entries) > 0:
            client.publish("margot/" + app_id + "/" + t_id + "/prediction", json.dumps(entries[0]["knowledge"]))


parser = argparse.ArgumentParser(
    description="Execute the Stub_agora component."
)
parser.add_argument("mqtt_broker_address", help="Address of the MQTT brokerpy", default="127.0.0.1")

args = parser.parse_args()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(args.mqtt_broker_address, 1883, 60)

client.loop_forever()

